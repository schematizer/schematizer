# Supported scalars

_([see more](https://graphql.org/learn/schema/#scalar-types))_
 - `'boolean'`: Boolean scalar (true / false)
 - `'float'`: Float scalar (number with decimals)
 - `'id'`: Id scalar (text)
 - `'int'`: Int scalar (integer number)
 - `'string'`: String scalar (text)