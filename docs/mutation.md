# Query
Te class `Mutations` allows to defin graphql mutations, the instances must be sended to `schematize` or `Schematizer.use`

## Declaration
The `Mutations` constructor receive an object argument, the object must contain query definitions in the keys using the `Fn` function ([see more](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/fn.md))

_**test:** simple declaration_
```ts
class ProductInput {}
class EmployeeInput {}
class Product {}
class Employee {}

const mutations = new Mutations({
  products: Fn(Product, ({
    input = Arg(ProductInput),
  }) => () => {
    return {};
  }),
  employees: Fn(Employee, ({
    input = Arg(EmployeeInput),
  }) => () => {
    return {};
  }),
});
```
