# Fn funtion

`Fn` is used to define queries and mutations, it has hard type validation of return and arguments, [see more about arguments](./args.md)

## Structure

the main structure is 

```ts
Fn(ReturnedType, Resolver)
```

### Returned type

The returned type must be `ScalarName | Class | [ScalarName | Class]`

_**Note**: the enums are not supported_

```ts
Fn('int', Resolver)
Fn(['float'], Resolver)
Fn(MyTypeClass, Resolver)
Fn([MyTypeInput], Resolver)
```

the supported scalars are

_([see more](https://graphql.org/learn/schema/#scalar-types))_
 - `'boolean'`: Boolean scalar (true / false)
 - `'float'`: Float scalar (number with decimals)
 - `'id'`: Id scalar (text)
 - `'int'`: Int scalar (integer number)
 - `'string'`: String scalar (text)

### Resolver
The resolver is a function (arguments) that returns other function (body)
```ts
Fn(ReturnedType, ArgsFunction => BodyFunction)
```

the ArgsFunction declares the arguments with `Arg` and return BodyFunction.

[see more about arguments](./args.md)

```ts
() => {
  anArgument = Arg('float'),
  otherArgument = Arg(['int']),
}) => BodyFunction
```

BodyFunction is called when the query or mutation is resolving, it receive as param an object with.

- **args:** resolver arguments (`object`)
- **context** graphql context (`object`)
- **info** graphql selection info ([`GraphQLResolveInfo`](https://graphql.org/graphql-js/type/#graphqlobjecttype))
- **name** resolver name (`string`)
- **selection** fields information of query selection (`FunctionSelection`)
- **type** resolver type (`'query' | 'mutation' | 'field'`)

```ts
const queries = new Queries({
  getProduct: Fn([Product], ({
    id = Arg('int'),
  }) => ({
    info,
    name,
    type,
  }) => {
    console.log(`${name} ${type} is called`); // getProduct query is called
    console.log(context); // gql context
    console.log(info); // gql info

    // https://www.npmjs.com/package/@mando75/typeorm-graphql-loader
    const result = context.loader.loadOne(Product, { id }, info);

    return result as any;
  });
});
```

## Methods
when `Fn` is called returns a `FunctionDeclaration`, this class has the follownig methods

- *onSelect*: Add an event listener to be called before select
