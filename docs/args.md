# Arg

`Arg` is a function to declare an argument on a query/mutation or field resolve.
You need send the argument type to `Arg`

```ts
Arg('int')
Arg(MyInputClass)
Arg(['float'])
Arg([OtherInputClass])
```

you can set options using an object as argument

- **type**: (required) is the argument type
- **nullable**: set the argument as nullable

```ts
Arg({ type: 'int', nullable: true })
```
