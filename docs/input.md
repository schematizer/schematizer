# Input
To define inputs, we will use `Input` class and send our instances to `schematize` or `Schematizer.use`

## Declaration
the constructor argument must be a class that represents the graphql input, the instance has `fields` and `as` methods to set fields and alias.

_**test:** simple input_
```ts
class ToDo {
  public name: string;
  public description: string;
}

const toDoInput = new Input(ToDo)
  .as('ToDoInput')
  .fields(() => ({
    name: 'string',
    description: 'string',
  }));
```

## Fields
The input fields can be `ScalarName | [ScalarName] | Class | [Class]`

_**test:** field types_
```ts
class ToDoInput {}

class ToDoListInput {
  public name: string;
  public assignedTo: number[];
  public items: ToDoInput[];
}

const toDoListInput = new Input(ToDoListInput).fields(() => ({
  name: 'string',
  assignedTo: ['int'],
  items: [ToDoInput],
}));
```

## Field options

An input field can be defined as object to set more configurations, the object structure is

- **type**: (required): a field type
- **nullable**: marks the field as nullable

_**test:** field options_
```ts
class VariantInput {}

class ProductInput {
  public name: string;
  public variants: VariantInput[];
}

const productInput = new Input(ProductInput).fields(() => ({
  name: {
    type: 'string',
  },
  variants: {
    type: [VariantInput],
    nullable: true,
  },
}));
```