# Type
To define a graphql type, we need instantiate `Type` class and put it as parameter to `schematize` or `Schematizer.use`.

## Declaration
The `Type` class constructor receive a class as argument, the instance will has `fields` and `as` methods to define the fields and an alias.

___test:__ simple type_
```ts
class Recipe {
  public id: string;
  public title: string;
  public views: number;
  public visible: boolean;
}

const recipeType = new Type(Recipe)
  .as('RecipeType')
  .fields(() => ({
    id: 'id',
    title: 'string',
    views: 'int',
    visible: 'boolean',
  }));
```

## Fields

A field can be an scalar or an array of a scalar or a class or an array of a class (`ScalarName | [ScalarName] | Class | [Class]`)

___test:__ field types_
```ts
class Ingredient {}
class User {}

class Recipe {
  public title: string;
  public owner: User;
  public tags: string[];
  public ingredients: Ingredient[];
}

const recipeType = new Type(Recipe).fields(() => ({
  title: 'string', // scalar
  owner: User, // nested type
  tags: ['string'], // array of scalars
  ingredients: [Ingredient], // array of typed
}));
```

## Field options

An type field can be an object instead of a field type, the object structure is

- type (required): a field type
- nullable: marks the field as nullable
- resolve: function to resolve the field value
- onSelect: event function or array of event functions to trigger when field is selected

___test:__ field options_
```ts
class Charger {}

class Phone {
  public name: string;
  public charger: Charger;
}

const phoneType = new Type(Phone).fields(() => ({
  name: {
    type: 'string',
  },
  charger: {
    type: Charger,
    nullable: true,
  },
}));
```