# Schematizer
`Schematizer` is the main class of this library, here you will put the definitions to schematize.

## Methods
- `use: (...schematizables: Schematizable[]) => Schematizer`: add unordered list of types, inputs, queries, mutations and plugins

- `schematize: () => GraphQLSchema`: make a graphql schema with the definitions specified with `use` method

- `onSelect: (listener: OnSelectEvent) => Schematizer`: listen for query or mutation request

- `onResolve: (listener: OnResolveEvent) => Schematizer`: listen for resolved mutation or request, if this listener return a value, it will be the new resolved value

- `onSchematize: () => (schematizer: Schematizer) => void | Promise<void>` listen event, listeners will be fired after schematize

## Plugin
A plugin is simply a function that receive a schematizer as argument, you can wrap your listeners or definitions with plugins

```ts
const plugin = (schematizer: Schematizer) => {
  console.log(schematizer);
};

new Schematizer().use(plugin);
```
