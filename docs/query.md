# Query
Te `Queries` class defines graphql queries, the instances must be sended to `schematize` or `Schematizer.use`

## Declaration
The `Queries` constructor receive an object argument, the object must contain query definitions in the keys using the `Fn` function ([see more](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/fn.md))

_**test:** simple declaration_
```ts
class Product {}
class Employee {}

const queries = new Queries({
  products: Fn([Product], () => () => [{}, {}, {}, {}]),
  employees: Fn([Employee], () => () => [{}, {}, {}, {}]),
});
```
