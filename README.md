# Schematizer
This is another library for make graphql schemas with hard type declaration on arguments, types and inputs

## Installation
you can install it with yarn or npm
```
yarn add @schematizer/schematizer
```
or
```
npm install @schematizer/schematizer
```

## docs
- [Type](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/type.md)
- [Input](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/input.md)
- [Query](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/query.md)
- [Mutation](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/mutation.md)
- [Schematizer](https://gitlab.com/schematizer/schematizer/-/blob/master/docs/schematizer.md)

## Example
You can see [the quick start demo](https://gitlab.com/schematizer/examples/quick-start) or see the next code examplte to show how this library works

```ts
import { Fn, Queries, schematize, Type }  from '@schematizer/schematizer';
import { graphql } from 'graphql';

// make a type

class Product {
  public id: number;
  public name: string;
  public price: number;
  public salable: boolean;
}

const productType = new Type(Product).fields(() => ({
  id: 'int',
  name: 'string',
  price: 'float',
  salable: 'boolean',
}));

const products: Product[] = [
  { id: 1, name: 'foo', price: 5, salable: true },
  { id: 2, name: 'bar', price: 2, salable: true },
];

// make queries

const queries = new Queries({
  products: Fn([Product], () => () => products),
});

// schematize

const schema = schematize(productType, queries);
const source = `
  query {
    products {
      id
      name
      price
      salable
    }
  }
`;

// you can use your favorite server like "graphql yoga" or "apollo server"
const { data, errors } = await graphql({ schema, source });
```


## Type
The `Type` class defines a graphql type, first we need define a class that will represent our type and we will create a `Type` instance

```ts
import { Type } from '@schematizer/schematizer';

class Person {
  public id: number;
  public name: string;
}

const personType = new Type(Person)
  .as('userType') // optional
  .fields(() => ({
    id: 'int',
    name: 'string',
  }));

```

## Input
Similar to Type, we need define a class to represent an input
```ts
import { Input } from '@schematizer/schematizer';

class MovementInput {
  public from: number;
  public to: number;
  public amount: number;
  public description: string;
}

const movementInput = new Input(MovementInput).fields(() => ({
  from: 'int',
  to: 'int',
  amount: 'float',
  description: 'string',
}));
```

## Query
To define graphql queries we need use `Queries` class, we will define the queries in the constructor argument
```ts
import { Arg, Fn, Queries } from '@schematizer/schematizer';
import { Product } from '../types';
import { getProductsMagically, getProductMagically } from '../magic';

const queries = new Queries({
  getProducts: Fn([Product], ({
    sqlInject = Arg('string'), // do not do this
  }) => () => {
    return getProductsMagically(sqlInject);
  }),

  getProduct: Fn(Product, ({
    id = Arg('int'),
  }) => () => {
    return getProductMagically(id);
  }),
});
```

## Mutation
The mutations works just like queries, but with the `Mutations` class

```ts
import { Arg, Fn, Mutations } from '@schematizer/schematizer';
import { Product } from '../types';
import { ProductInput } from '../inputs';
import { addProductMagically, removeProductMagically } from '../magic';

const mutations = new Mutations({
  addProduct: Fn(Product, ({
    input = Arg(ProductInput),
  }) => () => {
    return addProductMagically(input);
  }),

  removeProduct: Fn(Product, ({
    id = Arg('int'),
  }) => () => {
    return removeProductMagically(id);
  }),
});
```
