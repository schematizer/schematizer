import {
  GraphQLInputObjectType,
  GraphQLInputType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLOutputType,
} from 'graphql';
import { Input, Type } from './definers';
import { InputSchematizer, TypeSchematizer } from './schematizers';
import { ClassDef, VarInfo } from './types';
import { getVarType, parseVar } from './utils/parseVarInfo';
import { resolveScalar } from './utils/resolveScalar';
import { separateSchematizables } from './utils/separateSchematizbles';

function wrapResolvedObject(info: VarInfo, resolved: any) {
  const result = info.type instanceof Array
    ? new GraphQLList(new GraphQLNonNull(resolved))
    : resolved;

  if (info.nullable) {
    return result;
  }
  return new GraphQLNonNull(result);
}

export class DefinitionStore {
  public readonly inputs: InputSchematizer[] = [];
  public readonly types: TypeSchematizer[] = [];

  public readonly resolvedInputs = new Map<InputSchematizer, GraphQLInputObjectType>();
  public readonly resolvedTypes = new Map<TypeSchematizer, GraphQLObjectType>();

  public add(...defs: Array<Input | Type>) {
    const { types, inputs } = separateSchematizables(defs);

    this.inputs.push(...inputs.map(input => new InputSchematizer(input)));
    this.types.push(...types.map(type => new TypeSchematizer(type)));

    return this;
  }

  public getInput(target: ClassDef | string) {
    if (typeof target === 'string') {
      return this.inputs.find(input => input.alias === target);
    }
    return this.inputs.find(input => input.target === target);
  }

  public getType(target: ClassDef | string) {
    if (typeof target === 'string') {
      return this.types.find(type => type.alias === target);
    }
    return this.types.find(type => type.target === target);
  }

  public resolveInput(declaration: VarInfo): GraphQLInputType {
    const info = parseVar(declaration);
    const type = getVarType(declaration);

    const resolved = type instanceof Function
      ? this.getOrResolveInput(type)
      : resolveScalar(type);

    return wrapResolvedObject(info, resolved);
  }

  public resolveOutput(declaration: VarInfo): GraphQLOutputType {
    const info = parseVar(declaration);
    const type = getVarType(declaration);

    const resolved = type instanceof Function
      ? this.getOrResolveType(type)
      : resolveScalar(type);

    return wrapResolvedObject(info, resolved);
  }

  private getOrResolveInput(target: ClassDef): GraphQLInputObjectType {
    const input = this.getInput(target);
    if (!input) {
      throw new Error(`"${target.name}" class was not registered as Input`);
    }

    const resolved = this.resolvedInputs.get(input);
    if (resolved) {
      return resolved;
    }

    const newResolved = input.schematize(this);
    this.resolvedInputs.set(input, newResolved);

    return newResolved;
  }

  private getOrResolveType(target: ClassDef): GraphQLObjectType {
    const type = this.getType(target);
    if (!type) {
      throw new Error(`"${target.name}" class was not registered as Type`);
    }

    const resolved = this.resolvedTypes.get(type);
    if (resolved) {
      return resolved;
    }

    const newResolved = type.schematize(this);
    this.resolvedTypes.set(type, newResolved);

    return newResolved;
  }
}
