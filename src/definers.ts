import {
  ClassDef,
  FieldSelectedEvent,
  FunctionArgs,
  FunctionResolver,
  InputFields,
  ResolverListener,
  TypeFields,
  TypeValue,
  ValueTypeDeclaration,
  VarDeclaration,
  VarInfo,
  VarType,
} from './types';
import { ArgumentsBuilder } from './utils/ArgumentsBuilder';
import { parseStrictVarInfo } from './utils/parseVarInfo';

// #region arg.ts
export function Arg<T extends VarType>(type: T | VarInfo<T>): TypeValue<T>;
export function Arg<T extends VarType>(type: [T] | VarInfo<[T]>): Array<TypeValue<T>>;

export function Arg(declaration: VarDeclaration) {
  const info = parseStrictVarInfo(declaration);
  Arg.definitions.push(info);
  return null as any;
}

Arg.definitions = [] as Array<Required<VarInfo>>;
//#endregion

// #region FunctionDeclaration.ts
export class FunctionDeclaration<Returns extends VarType = any> {
  public name: string;
  public arguments: FunctionArgs<any>;
  public returnedType: VarInfo<Returns>;
  public resolver: FunctionResolver<ValueTypeDeclaration<Returns>>;
  public _selectListeners: ResolverListener[] = [];

  constructor(
    returnType: Returns | VarInfo<Returns>,
    resolver: FunctionResolver<TypeValue<Returns>>,
  );

  constructor(
    returnType: [Returns] | VarInfo<[Returns]>,
    resolver: FunctionResolver<Array<TypeValue<Returns>>>,
  );

  constructor(
    returnType: VarDeclaration,
    resolver: FunctionResolver<any>,
  ) {
    const argumentsBuilder = new ArgumentsBuilder<VarInfo>(proxy => resolver(proxy));
    this.arguments = argumentsBuilder.build(Arg.definitions);
    Arg.definitions = [];
    this.returnedType = parseStrictVarInfo(returnType);
    this.resolver = resolver;
  }

  public onSelect(listener: ResolverListener) {
    this._selectListeners.push(listener);
    return this;
  }
}

export function Fn<Returns extends VarType>(
  returnType: Returns | VarInfo<Returns>,
  fn: FunctionResolver<TypeValue<Returns>>,
): FunctionDeclaration<Returns>;

export function Fn<Returns extends VarType>(
  returnType: [Returns] | VarInfo<[Returns]>,
  fn: FunctionResolver<Array<TypeValue<Returns>>>,
): FunctionDeclaration<Returns>;

export function Fn(returnType: VarDeclaration, func: FunctionResolver<any>) {
  return new FunctionDeclaration(returnType, func);
}
// #endregion

// #region FunctionsContainer.ts
export class FunctionsContainer {
  constructor(
    public functions: Record<string, FunctionDeclaration>,
  ) {
    // names assignament
    Object.entries(functions).forEach(([name, fn]) => {
      fn.name = name;
    });
  }

  public mergeWith(...others: FunctionsContainer[]) {
    others.forEach(other => {
      Object.entries(other.functions).forEach(([name, fn]) => {
        if (name in this.functions) {
          throw new Error(name);
        }
        this.functions[name] = fn;
      });
    });
  }
}
// #endregion

// #region Input.ts

export class Input<Target extends object = any> {
  public alias: string;
  public target: ClassDef<Target>;
  public getFields: () => InputFields<Target>;

  constructor(target: ClassDef<Target>) {
    this.target = target;
  }

  public as(alias: string) {
    this.alias = alias;
    return this;
  }

  public fields(getFields: () => InputFields<Target>) {
    this.getFields = getFields;
    return this;
  }
}
// #endregion

// #region Mutations.ts
export class Mutations extends FunctionsContainer {
  public functions: Record<string, FunctionDeclaration>;

  constructor(functions: Record<string, FunctionDeclaration>) {
    super(functions);
    this.functions = functions;
  }
}
// #endregion

// #region Queries.ts
export class Queries extends FunctionsContainer {
  public functions: Record<string, FunctionDeclaration>;

  constructor(functions: Record<string, FunctionDeclaration>) {
    super(functions);
    this.functions = functions;
  }
}
// #endregion

// #region Type.ts
export class Type<Target extends object = any> {
  public alias: string;
  public target: ClassDef<Target>;
  public getFields: () => TypeFields<Target>;
  public _selectListeners: Array<FieldSelectedEvent<Target>> = [];

  constructor(target: ClassDef<Target>) {
    this.target = target;
  }

  public as(alias: string) {
    this.alias = alias;
    return this;
  }

  public fields(getFields: () => TypeFields<Target>) {
    this.getFields = getFields;
    return this;
  }

  public onSelect(listener: FieldSelectedEvent<Target>) {
    this._selectListeners.push(listener);
    return this;
  }
}
// #endregion
