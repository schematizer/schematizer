import { GraphQLObjectType, GraphQLSchema } from 'graphql';
import { Mutations, Queries } from './definers';
import { DefinitionStore } from './DefinitionStore';
import { triggerListeners } from './schemaEvents';
import { schematizeFunctions } from './schematizers/schematizeFunctions';
import { ResolverInfo } from './types';
import { Schematizable, separateSchematizables } from './utils/separateSchematizbles';

export type OnSelectEvent = (ctx: ResolverInfo) => Promise<void> | void;
export type OnResolveEvent = (ctx: ResolverInfo & { resolved: any } ) => any;
export type OnSchematizeEvent = (schematizer: Schematizer) => void | Promise<void>;

export type SchematizerPlugin = (schematizer: Schematizer) => void;

export class Schematizer {
  public readonly store: DefinitionStore = new DefinitionStore();
  public readonly mutations: Mutations = new Mutations({});
  public readonly queries: Queries = new Queries({});
  public readonly onSelectListeners: OnSelectEvent[] = [];
  public readonly onResolveListeners: OnResolveEvent[] = [];
  public readonly onSchematizeListeners: OnSchematizeEvent[] = [];

  constructor() {
    this.onSelect = this.onSelect.bind(this);
    this.onResolve = this.onResolve.bind(this);
    this.onSchematize = this.onSchematize.bind(this);
    this.schematize = this.schematize.bind(this);
    this.triggerOnResolve = this.triggerOnResolve.bind(this);
    this.triggerOnSelect = this.triggerOnSelect.bind(this);
    this.use = this.use.bind(this);
  }

  public use(...schematizables: Schematizable[]) {
    const {
      inputs,
      mutations,
      plugins,
      queries,
      types,
    } = separateSchematizables(schematizables);

    this.store.add(
      ...inputs,
      ...types,
    );
    this.mutations.mergeWith(...mutations);
    this.queries.mergeWith(...queries);

    plugins.forEach(plugin => plugin(this));

    return this;
  }

  public onSelect(listener: OnSelectEvent) {
    this.onSelectListeners.push(listener);
    return this;
  }

  public onResolve(listener: OnResolveEvent) {
    this.onResolveListeners.push(listener);
    return this;
  }

  public onSchematize(listener: OnSchematizeEvent) {
    this.onSchematizeListeners.push(listener);
    return this;
  }

  public async schematize() {
    const queries = schematizeFunctions({
      functions: this.queries.functions,
      onResolve: this.triggerOnResolve,
      onSelect: this.triggerOnSelect,
      resolverType: 'query',
      store: this.store,
    });

    const mutations = schematizeFunctions({
      functions: this.mutations.functions,
      onResolve: this.triggerOnResolve,
      onSelect: this.triggerOnSelect,
      resolverType: 'mutation',
      store: this.store,
    });

    for (const listener of this.onSchematizeListeners) {
      await listener(this);
    }

    const query = !queries ? null : new GraphQLObjectType({
      name: 'query',
      fields: queries,
    });

    const mutation = !mutations ? null : new GraphQLObjectType({
      name: 'mutation',
      fields: mutations,
    });

    return new GraphQLSchema({ query, mutation });
  }

  private async triggerOnSelect(resolverInfo: ResolverInfo) {
    for (const onSelect of this.onSelectListeners) {
      await onSelect(resolverInfo);
    }
    await triggerListeners(resolverInfo);
  }

  private async triggerOnResolve(resolverInfo: ResolverInfo & { resolved: any }) {
    let resolved = resolverInfo.resolved;
    for (const onResolve of this.onResolveListeners) {
      const result = await onResolve({ ...resolverInfo, resolved });
      if (result !== undefined) {
        resolved = result;
      }
    }

    return resolved;
  }
}

export function schematize(...usables: Schematizable[]) {
  const schematizer = new Schematizer().use(...usables);
  return schematizer.schematize();
}
