import { GraphQLFieldConfigArgumentMap } from 'graphql';
import { DefinitionStore } from '../DefinitionStore';
import { FunctionArgs } from '../types';

export interface SchematizeFunctionArgumentsArg {
  args: FunctionArgs;
  store: DefinitionStore;
}

export function schematizeFunctionArguments({
  args,
  store,
}: SchematizeFunctionArgumentsArg): GraphQLFieldConfigArgumentMap | undefined {
  const entries = Object.entries(args);
  if ( entries.length === 0) return undefined;

  return entries.reduce((result, [name, info]) => {
    return {
      ...result,
      [name]: {
        type: store.resolveInput(info),
      },
    };
  }, {} as GraphQLFieldConfigArgumentMap);
}
