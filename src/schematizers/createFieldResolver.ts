import { GraphQLFieldResolver } from 'graphql';
import { FieldResolver, FieldResolverInfo, TypeField } from '../types';

export function createFieldResolver(
  name: string,
  fieldInfo: TypeField,
  // schematizer: Schematizer,
): GraphQLFieldResolver<any, any> | undefined {
  const { resolve } = fieldInfo;
  if (!resolve) {
    return undefined;
  }

  return (source, args, context, info) => {
    const resolverInfo: FieldResolverInfo<any> = {
      args,
      context,
      info,
      name,
      root: source,
      // TODO: add selection
      selection: undefined as any,
      type: 'field',
    };

    // call resolver
    const resolver = resolve(args);
    const resolved = resolver(resolverInfo);

    return resolved;
  };
}
