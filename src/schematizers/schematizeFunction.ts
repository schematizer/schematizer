import { GraphQLFieldConfig, GraphQLFieldResolver } from 'graphql';
import { FunctionDeclaration } from '../definers';
import { DefinitionStore } from '../DefinitionStore';
import { OnResolveEvent, OnSelectEvent } from '../Schematizer';
import { buildSelectionData } from '../selection';
import { ResolverInfo, ResolverType } from '../types';
import { schematizeFunctionArguments } from './schematizeFunctionArguments';

export interface SchematizeFunctionArg {
  fnDeclaration: FunctionDeclaration;
  store: DefinitionStore;
  onResolve: OnResolveEvent;
  onSelect: OnSelectEvent;
  resolverType: ResolverType;
}

export function schematizeFunction({
  fnDeclaration,
  onResolve,
  onSelect,
  store,
  resolverType,
}: SchematizeFunctionArg): GraphQLFieldConfig<any, any> {
  const resolve: GraphQLFieldResolver<any, any> = async (source, args, context, info) => {
    const resolverInfo: ResolverInfo = {
      args,
      context,
      info,
      name: fnDeclaration.name,
      selection: buildSelectionData({
        fn: fnDeclaration,
        info,
        store,
      }),
      type: resolverType,
    };

    await onSelect(resolverInfo);
    const resolved = fnDeclaration.resolver(args)(resolverInfo);
    const result = await onResolve({ ...resolverInfo, resolved });

    return result;
  };

  // const fnReturnType = fnDeclaration.returnedType.type;

  return {
    // typeInfo: fnReturnType instanceof Function
    //   ? store.getType(fnDeclaration.returnedType.type)
    //   : (null as any),
    type: store.resolveOutput(fnDeclaration.returnedType),
    args: schematizeFunctionArguments({ store, args: fnDeclaration.arguments }),
    resolve,
  };
}
