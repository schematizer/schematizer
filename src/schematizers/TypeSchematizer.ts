import { GraphQLFieldConfig, GraphQLFieldConfigMap, GraphQLObjectType } from 'graphql';
import { ClassDef } from '../';
import { Type } from '../definers';
import { DefinitionStore } from '../DefinitionStore';
import {
  FieldSelectedEvent,
  StrictTypeField,
  TypeField,
  TypeFieldDeclaration,
} from '../types';
import { parseStrictTypeField } from '../utils/parseTypeFieldInfo';
import { createFieldResolver } from './createFieldResolver';

export type InternalTypeFields = Record<string, StrictTypeField>;

export class TypeSchematizer {
  public alias: string;
  public fields: InternalTypeFields;
  public target: ClassDef;
  public _selectListeners: Array<FieldSelectedEvent<any>>;

  constructor(type: Type) {
    this.target = type.target;
    this.alias = type.alias || type.target.name;
    this._selectListeners = type._selectListeners;

    const entries: Array<[string, TypeFieldDeclaration | null]>
      = Object.entries(type.getFields());

    this.fields = entries.reduce((result, [name, declaration]) => {
      if (declaration === null) return result;
      const info = parseStrictTypeField(declaration);

      return {
        ...result,
        [name]: info,
      };
    }, {} as InternalTypeFields);
  }

  public resolveFields(store: DefinitionStore): GraphQLFieldConfigMap<any, any> {
    const fields: Array<[string, TypeField]> = Object.entries(this.fields);

    return fields.reduce((result, [fieldName, fieldInfo]) => ({
      ...result,
      [fieldName]: this.resolveField(
        fieldName,
        fieldInfo,
        store,
      ),
    }), {} as GraphQLFieldConfigMap<any, any>);
  }

  public schematize(resolver: DefinitionStore) {
    return new GraphQLObjectType({
      name: this.alias,
      fields: () => this.resolveFields(resolver),
    });
  }

  private resolveField(
    name: string,
    info: TypeField,
    store: DefinitionStore,
  ): GraphQLFieldConfig<any, any> {
    const fieldConfig: GraphQLFieldConfig<any, any> = {
      type: store.resolveOutput(info),
      resolve: createFieldResolver(name, info),
    };

    return fieldConfig;
  }
}
