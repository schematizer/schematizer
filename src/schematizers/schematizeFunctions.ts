import { FunctionDeclaration } from '../definers';
import { DefinitionStore } from '../DefinitionStore';
import { OnResolveEvent, OnSelectEvent } from '../Schematizer';
import { ResolverType } from '../types';
import { schematizeFunction } from './schematizeFunction';

export interface SchematizeFunctionsArg {
  functions: Record<string, FunctionDeclaration>;
  store: DefinitionStore;
  onSelect: OnSelectEvent;
  onResolve: OnResolveEvent;
  resolverType: ResolverType;
}

export function schematizeFunctions({
  functions,
  ...info
}: SchematizeFunctionsArg) {
  const entries = Object.entries(functions);
  if (!entries || entries.length === 0) return null;

  const schematized = entries.reduce((result, [name, func]) => ({
    ...result,
    [name]: schematizeFunction({ ...info, fnDeclaration: func }),
  }), {});

  return schematized;
}
