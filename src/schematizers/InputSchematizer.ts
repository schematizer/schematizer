import { GraphQLInputFieldConfig, GraphQLInputFieldConfigMap, GraphQLInputObjectType } from 'graphql';
import { ClassDef, VarInfo } from '../';
import { Input } from '../definers';
import { DefinitionStore } from '../DefinitionStore';
import { InputFieldDeclaration } from '../types';
import { parseStrictVarInfo } from '../utils/parseVarInfo';

export interface StrictInputfield extends Required<VarInfo> {
}

export type InternalInputFields = Record<string, StrictInputfield>;

export class InputSchematizer {
  public alias: string;
  public fields: InternalInputFields;
  public target: ClassDef;

  constructor(input: Input) {
    this.target = input.target;
    this.alias = input.alias || input.target.name;

    const entries: Array<[string, InputFieldDeclaration | null]>
      = Object.entries(input.getFields());

    this.fields = entries.reduce((result, [name, declaration]) => {
      if (declaration === null) return result;
      const info = parseStrictVarInfo(declaration);

      return {
        ...result,
        [name]: info,
      };
    }, {} as InternalInputFields);
  }

  public schematize(resolver: DefinitionStore) {
    return new GraphQLInputObjectType({
      name: this.alias,
      fields: () => this.resolveFields(resolver),
    });
  }

  public resolveFields(resolver: DefinitionStore): GraphQLInputFieldConfigMap {
    const fields: Array<[string, VarInfo]> = Object.entries(this.fields);

    return fields.reduce((result, [fieldName, fieldInfo]) => {
      const fieldConfig: GraphQLInputFieldConfig = {
        type: resolver.resolveInput(fieldInfo),
      };

      return {
        ...result,
        [fieldName]: fieldConfig,
      };
    }, {} as GraphQLInputFieldConfigMap);
  }
}
