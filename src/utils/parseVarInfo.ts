import { VarDeclaration, VarInfo, VarType } from '../';

export function getVarType(declaration: VarDeclaration): VarType {
  if (
    declaration instanceof Function ||
    declaration === 'boolean' ||
    declaration === 'float' ||
    declaration === 'id' ||
    declaration === 'int' ||
    declaration === 'string'
  ) {
    return declaration;
  } else if (declaration instanceof Array) {
    return declaration[0];
  } else if ('type' in declaration) {
    return getVarType(declaration.type);
  } else {
    throw new Error(`Unexpected var type: ${declaration}`);
  }
}

export function parseVar(declaration: VarDeclaration): VarInfo {
  if (
    declaration instanceof Array ||
    declaration instanceof Function ||
    declaration === 'boolean' ||
    declaration === 'float' ||
    declaration === 'id' ||
    declaration === 'int' ||
    declaration === 'string'
  ) {
    return { type: declaration, nullable: false };
  } else if ('type' in declaration) {
    return declaration;
  } else {
    throw new Error(`Unexpected var type: ${declaration}`);
  }
}

// parser for function arguments/return types and input fields (create new types when any require
// new fields)
export function parseStrictVarInfo(
  declaration: VarDeclaration,
): Required<VarInfo> {
  const info = parseVar(declaration);

  return {
    type: info.type,
    nullable: Boolean(info.nullable),
  };
}
