import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLScalarType,
  GraphQLString,
} from 'graphql';
import { ValueType } from '../';

export function resolveScalar(scalarType: ValueType<any>): GraphQLScalarType {
  switch (scalarType) {
    case 'boolean':
      return GraphQLBoolean;
    case 'float':
      return GraphQLFloat;
    case 'id':
      return GraphQLID;
    case 'int':
      return GraphQLInt;
    case 'string':
      return GraphQLString;
    default:
      throw new Error(`Unexpected field type "${scalarType}"`);
  }
}
