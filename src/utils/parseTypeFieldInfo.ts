import { VarType } from '../';
import { Arrayable, OneOrMore } from '../typeHelpers';
import { StrictTypeField, TypeField, TypeFieldDeclaration } from '../types';

export function parseTypeField(declaration: TypeFieldDeclaration): TypeField {
  if (
    declaration instanceof Array ||
    declaration instanceof Function ||
    declaration === 'boolean' ||
    declaration === 'float' ||
    declaration === 'id' ||
    declaration === 'int' ||
    declaration === 'string'
  ) {
    return { type: declaration, nullable: false };
  } else if ('type' in declaration) {
    return declaration;
  } else {
    throw new Error(`Unexpected var type: ${declaration}`);
  }
}

function oneOrMoreToArray<Type>(source: OneOrMore<Type>) {
  if (source instanceof Array) {
    return [...source];
  } else if (source) {
    return [source];
  }
  return [];
}

export function parseStrictTypeField<Root, Type extends Arrayable<VarType>>(
  declaration: TypeFieldDeclaration<Root, Type>,
): StrictTypeField {
  const info = parseTypeField(declaration);

  // NOTE: Add here the new FieldInfo properties
  return {
    ...info,
    type: info.type,
    resolve: info.resolve,
    nullable: Boolean(info.nullable),
    onSelect: oneOrMoreToArray(info.onSelect as any),
  };
}
