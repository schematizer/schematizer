export class ArgumentsBuilder<Value> {
  private fieldNames: string[] = [];

  constructor(call: (proxy: any) => void) {
    const proxy = new Proxy({}, {
      get: (target, prop: string) => {
        this.fieldNames.push(prop);
      },
    });
    call(proxy);
  }

  public build(values: Value[]): Record<string, Value> {
    return this.fieldNames.reduce((result, name, index) => {
      return {
        ...result,
        [name]: values[index],
      };
    }, {});
  }
}
