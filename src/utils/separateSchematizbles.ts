import { Input, Mutations, Queries, Type } from '../definers';
import { SchematizerPlugin } from '../Schematizer';

export type Schematizable = Input | Mutations | Queries | SchematizerPlugin | Type;

export function separateSchematizables(usables: Schematizable[]) {
  const inputs: Input[] = usables.filter((e): e is Input => e instanceof Input);
  const mutations: Mutations[] = usables.filter((e): e is Mutations => e instanceof Mutations);
  const plugins: SchematizerPlugin[] =
    usables.filter((e): e is SchematizerPlugin => e instanceof Function);
  const queries: Queries[] = usables.filter((e): e is Queries => e instanceof Queries);
  const types: Type[] = usables.filter((e): e is Type => e instanceof Type);

  return {
    inputs,
    mutations,
    plugins,
    queries,
    types,
  };
}
