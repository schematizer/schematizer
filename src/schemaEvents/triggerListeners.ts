import { ResolverInfo } from '../types';
import { triggerFieldListeners } from './triggerFieldListeners';

export async function triggerListeners(resolverInfo: ResolverInfo) {
  const { selection: { selection, fn  } } = resolverInfo;

  for (const listener of fn._selectListeners) {
    await listener(resolverInfo);
  }

  await triggerFieldListeners(resolverInfo.context, selection);
}
