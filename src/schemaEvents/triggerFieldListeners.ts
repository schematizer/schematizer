import { FieldSelection } from '../selection';
import { GraphqlContext } from '../types';

export async function triggerFieldListeners(context: GraphqlContext, selection: FieldSelection) {
  const { info, fields, type } = selection;

  for (const onSelect of info.onSelect) {
    await onSelect({
      context,
      selection,
    });
  }

  if (!fields || typeof type === 'string') {
    return;
  }

  for (const onSelect of type._selectListeners) {
    await onSelect({
      context,
      selection,
    });
  }

  for (const fieldName of Object.keys(fields)) {
    await triggerFieldListeners(context, fields[fieldName]);
  }
}
