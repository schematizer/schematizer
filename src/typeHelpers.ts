// - Helpers
export type OneOrMore<T> = T | T[];
export type Arrayable<T> = T | [T];

// #region https://medium.com/dailyjs/typescript-create-a-condition-based-subset-types-9d902cea5b8c
type FilterFlags<Base, Condition> = {
  [Key in keyof Base]: Base[Key] extends Condition ? never : Key
};
export type AllowedNames<Base, Condition> = FilterFlags<Base, Condition>[keyof Base];
// export type InvalidFieldTypes = (...args: any[]) => any | Date | symbol | undefined;

export type RecursivePartial<T> = {
  [P in keyof T]?:
    T[P] extends Array<infer U> ? Array<RecursivePartial<U>> :
    T[P] extends object ? RecursivePartial<T[P]> :
    T[P];
};
