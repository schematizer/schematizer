import { GraphQLResolveInfo } from 'graphql';
import { FieldSelection, FunctionSelection } from './selection';
import { Arrayable, OneOrMore, RecursivePartial } from './typeHelpers';

export type ScalarName = 'boolean' | 'float' | 'id' | 'int' | 'string';
export type ClassDef<T = any> = new (...args: any[]) => T;
export type VarType<Type = any> = ScalarName | ClassDef<Type>;

export interface VarInfo<Type extends Arrayable<VarType> = any> {
  type: Type;
  nullable?: boolean;
}

export type VarDeclaration = VarType | [VarType] | VarInfo;

// - - Parsing

export type ValueType<Type> =
  Type extends boolean ? 'boolean' :
  Type extends number ? ('float' | 'int') :
  Type extends string ? ('id' | 'string') :
  ClassDef<Type>
;

export type ValueTypeDeclaration<Type extends Arrayable<VarType>> =
  Type extends [VarType] ? Array<ValueType<Type[0]>> :
  Type extends VarType ? ValueType<VarType> :
  never
;

// thanks men
// https://stackoverflow.com/questions/59381974/typescript-conditional-type-does-not-work

type ScalarValue<Type extends ScalarName> = {
  boolean: boolean,
  float: number,
  int: number,
  id: string,
  string: string,
}[Type];

export type TypeValue<Type extends VarType> =
  Type extends ScalarName ? ScalarValue<Type> :
  Type extends ClassDef ? InstanceType<Type> :
  never
;

export type TypeValueDeclaration<Type extends Arrayable<VarType>> =
  Type extends [VarType] ? Array<TypeValue<Type[0]>> :
  Type extends VarType ? TypeValue<Type> :
  never
;

/*tslint:disable no-empty-interface*/
/**
 * Override this with your custom fields
 */
export interface GraphqlContext {}

export type ResolverType = 'field' | 'mutation' | 'query';

// -- Resolvers

export type ResolverReturn<T> = RecursivePartial<T> | T | null | undefined;

export interface ResolverInfo {
  args: Record<string, any>;
  context: GraphqlContext;
  info: GraphQLResolveInfo;
  name: string;
  selection: FunctionSelection;
  type: ResolverType;
}

// export type ResolverAlterator<T> = (value: T, info: ResolverInfo) => T | Promise<T>;
export type ResolverListener = (info: ResolverInfo) => void | Promise<void>;

export type FunctionResolver<T> =
  (args?: unknown) =>
  (info: ResolverInfo) =>
  (ResolverReturn<T> | Promise<ResolverReturn<T>>);

export type FunctionArgDeclaration<Type = any> = Type extends any[]
  ? VarInfo<[ValueType<Type[number]>]>
  : VarInfo<ValueType<Type>>
;

export type FunctionArgs<Type = any> = {
  [Field in keyof Type]: FunctionArgDeclaration<Type[Field]>;
};

// - - Input

export interface InputField<Field extends Arrayable<VarType<any>>> extends VarInfo<Field> {
}

export type InputFieldDeclaration<Field = any> = Field extends any[]
  ? ([ValueType<Field[number]>] | VarInfo<[ValueType<Field[number]>]>)
  : (ValueType<Field> | VarInfo<ValueType<Field>>)
;

export type InputFields<Target> = {
  [Field in keyof Target]: InputFieldDeclaration<Target[Field]> | null;
};

// - - Type

export interface FieldResolverInfo<Target> extends ResolverInfo {
  // data: FieldData[];
  root: Target;
}

export type FieldResolver<Target, T> =
  (args?: unknown) =>
  (data: FieldResolverInfo<Target>) =>
  (ResolverReturn<T> | Promise<ResolverReturn<T>>);

export interface FieldSelectedInfo {
  context: GraphqlContext;
  selection: FieldSelection;
}

export type FieldSelectedEvent<Target> =
  (data: FieldSelectedInfo) => void | Promise<void>;

export interface TypeField<
  Target = any,
  Field extends Arrayable<VarType<any>> = any,
> extends VarInfo<Field> {
  resolve?: FieldResolver<Target, TypeValueDeclaration<Field>>;
  onSelect?: OneOrMore<FieldSelectedEvent<Target>>;
}

export interface StrictTypeField<
  Target = any,
  Field extends Arrayable<VarType<any>> = any,
> extends TypeField<Target, Field> {
  onSelect: Array<FieldSelectedEvent<Target>>;
}

export type TypeFieldDeclaration<Target = any, Field = any> = Field extends any[]
  ? ([ValueType<Field[number]>] | TypeField<Target, [ValueType<Field[number]>]>)
  : (ValueType<Field> | TypeField<Target, ValueType<Field>>)
;
export type TypeFields<Target extends object> = {
  [Field in keyof Target]: TypeFieldDeclaration<Target, Target[Field]> | null;
};
