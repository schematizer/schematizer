import { FieldNode, GraphQLResolveInfo } from 'graphql';
import { FunctionDeclaration } from '../definers';
import { DefinitionStore } from '../DefinitionStore';
import { buildFunctionSelection, FunctionSelection } from './buildFunctionSelection';

export interface BuildSelectionDataArg {
  info: GraphQLResolveInfo;
  fn: FunctionDeclaration;
  store: DefinitionStore;
}

export function buildSelectionData({
  fn,
  info,
  store,
}: BuildSelectionDataArg): FunctionSelection {
  const { selectionSet } = info.operation;

  const selection = selectionSet.selections.find((sel: FieldNode) => {
    return sel.name.value === info.fieldName;
  }) as FieldNode;

  return buildFunctionSelection({ selection, fn, store });
}
