import { SelectionSetNode } from 'graphql';
import { ScalarName } from '../';
import { DefinitionStore } from '../DefinitionStore';
import { TypeSchematizer } from '../schematizers';
import { StrictTypeField } from '../types';
import { getVarType } from '../utils/parseVarInfo';

export interface FieldSelection {
  info: StrictTypeField;
  type: TypeSchematizer | ScalarName;
  fields?: Record<string, FieldSelection>;
  raw: SelectionSetNode | undefined;
}

export interface BuildFieldSelectionArg {
  info: StrictTypeField;
  store: DefinitionStore;
  selectionSet: SelectionSetNode | undefined;
}

export function buildFieldSelection({
  info,
  store,
  selectionSet,
}: BuildFieldSelectionArg): FieldSelection {
  const varType = getVarType(info);
  const type = typeof varType === 'string'
    ? varType
    : (store.getType(varType) || new Error(`Can not found "${varType.name}" type`));

  if (type instanceof Error) {
    throw type;
  }

  if (typeof type === 'string') {
    return {
      info,
      type,
      raw: selectionSet,
    };
  }

  if (!selectionSet) {
    throw new Error(`"selectionSet" can not be undefined`);
  }

  const graphqlSelections = selectionSet.selections;
  const fields = type.fields;

  const selection: Required<FieldSelection> = {
    info,
    type,
    fields: graphqlSelections.reduce((result, fieldSelection) => {
      if (fieldSelection.kind !== 'Field') {
        throw new Error(`unexpected ${fieldSelection.kind} kind`);
      }

      const fieldName = fieldSelection.name.value;
      if (!(fieldName in fields)) {
        throw new Error(`Can not find "${fieldName}" on "${type.alias}" type`);
      }
      const field = fields[fieldName];

      return {
        ...result,
        [fieldName]: buildFieldSelection({
          info: field,
          selectionSet: fieldSelection.selectionSet,
          store,
        }),
      };
    }, {}),
    raw: selectionSet,
  };

  return selection;
}
