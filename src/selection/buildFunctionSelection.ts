import { SelectionNode } from 'graphql';
import { FunctionDeclaration } from '../definers';
import { DefinitionStore } from '../DefinitionStore';
import { parseStrictTypeField } from '../utils/parseTypeFieldInfo';
import { buildFieldSelection, FieldSelection } from './buildFieldSelection';

export interface FunctionSelection {
  fn: FunctionDeclaration;
  raw: SelectionNode;
  selection: FieldSelection;
}

export interface BuildFunctionSelectionArg {
  selection: SelectionNode;
  fn: FunctionDeclaration;
  store: DefinitionStore;
}

export function buildFunctionSelection({
  selection,
  fn,
  store,
}: BuildFunctionSelectionArg): FunctionSelection {
  // query/mutation is considered as a "Field" selection
  if (selection.kind !== 'Field') {
    throw new Error(`unexpected "${selection.kind}" kind`);
  }

  // get the returned type of function
  const info = parseStrictTypeField(fn.returnedType);

  return {
    fn,
    selection: buildFieldSelection({
      info,
      store,
      selectionSet: selection.selectionSet,
    }),
    raw: selection,
  };
}
