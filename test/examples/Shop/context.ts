import { Data } from './data';

declare module '../../../src/types' {
  interface GraphqlContext {
    data: Data;
  }
}

export interface Context {
  data: Data;
}
