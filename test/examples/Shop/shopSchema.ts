import { Schematizer } from '../../../src';
import './context';
import inputs from './inputs';
import mutations from './mutations';
import queries from './queries';
import types from './types';

export function shopSchema(schematizer: Schematizer) {
  schematizer.use(
    ...types,
    ...inputs,
    ...queries,
    ...mutations,
  );
}
