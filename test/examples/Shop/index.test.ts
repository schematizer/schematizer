import { graphql } from 'graphql';
import { schematize, Schematizer } from '../../../src';
import { Data } from './data';
import { EmployeeInput, SaleInput } from './inputs';
import { shopSchema } from './shopSchema';
import { EmployeeType, SaleType } from './types';

describe('Shop', () => {
  const schema = schematize(shopSchema);

  it('show existent sales', async () => {
    const source = `
      query {
        salesList {
          id
          by {
            id
            name
          }
          products {
            id
            name
            quantity
          }
        }
      }
    `;
    const contextValue = {
      data: new Data({
        employees: [
          { id: 1, name: 'Me' },
        ],
        sales: [
          { id: 1, employeeId: 1 },
        ],
        soldProducts: [
          { id: 1, name: 'Foo', quantity: 10, saleId: 1 },
          { id: 2, name: 'Bar', quantity: 2, saleId: 1 },
        ],
      }),
    };

    const { data, errors } = await graphql({ contextValue, schema, source });

    const salesList: SaleType[] = [
      {
        id: 1,
        by: {
          id: 1,
          name: 'Me',
        },
        products: [
          { id: 1, name: 'Foo', quantity: 10 },
          { id: 2, name: 'Bar', quantity: 2 },
        ],
      },
    ];

    expect(data).toEqual({ salesList });
    expect(errors).toBeUndefined();
  });

  it('add employees', async () => {
    const data = new Data();
    const input: EmployeeInput = {
      name: 'I',
    };
    const source = `
      mutation($input: EmployeeInput!) {
        employeeAdd(input: $input) {
          id
          name
        }
      }
    `;
    const variableValues = { input };
    const contextValue = { data };

    const result = await graphql({
      schema,
      source,
      contextValue,
      variableValues,
    });

    expect(result.errors).toBeUndefined();
    expect(result.data).toEqual({
      employeeAdd: {
        id: 1,
        name: 'I',
      },
    });

    expect(data).toEqual({
      employees: [
        { id: 1, name: 'I' },
      ],
      sales: [],
      soldProducts: [],
    });
  });

  it('add sale', async () => {
    const data = new Data({
      employees: [
        { id: 100, name: 'Ray' },
      ],
    });
    const input: SaleInput = {
      by: 100,
      products: [
        { name: 'foo', quantity: 1 },
        { name: 'bar', quantity: 2 },
      ],
    };
    const source = `
      mutation($input: SaleInput!) {
        saleAdd(input: $input) {
          id
          by {
            id
            name
          }
          products {
            id
            name
            quantity
          }
        }
      }
    `;
    const variableValues = { input };
    const contextValue = { data };

    const result = await graphql({
      schema,
      source,
      contextValue,
      variableValues,
    });

    expect(result.errors).toBeUndefined();
    expect(result.data).toEqual({
      saleAdd: {
        id: 1,
        by : {
          id: 100,
          name: 'Ray',
        },
        products: [
          { id: 1, name: 'foo', quantity: 1 },
          { id: 2, name: 'bar', quantity: 2 },
        ],
      },
    });

    expect(data).toEqual({
      employees: [
        { id: 100, name: 'Ray' },
      ],
      sales: [
        { id: 1, employeeId: 100 },
      ],
      soldProducts: [
        { id: 1, name: 'foo', quantity: 1, saleId: 1 },
        { id: 2, name: 'bar', quantity: 2, saleId: 1 },
      ],
    });
  });

  it('check schematizer events', async () => {
    let counter = 0;
    let onSelectSecuence = 0;
    let onResolveSecuence = 0;
    let onResolveTwoSecuence = 0;

    const data = new Data({
      employees: [
        { id: 1, name: 'Ray' },
      ],
    });

    const schemaWithListeners = new Schematizer()
      .use(shopSchema)
      .onSelect(({ context, name, type }) => {
        onSelectSecuence = ++counter;
        expect(context).toEqual({ data });
        expect(name).toBe('employeeList');
        expect(type).toBe('query');
      })
      .onResolve(({ context, name, type, resolved }) => {
        onResolveSecuence = ++counter;
        expect(context).toEqual({ data });
        expect(name).toBe('employeeList');
        expect(type).toBe('query');
        expect(resolved).toEqual([{ id: 1, name: 'Ray' }]);

        // modify resolved value
        return resolved.map((employee: EmployeeType) => ({
          ...employee,
          name: employee.name.toUpperCase(),
        }));
      })
      .onResolve(({ resolved }) => {
        onResolveTwoSecuence = ++counter;
        expect(resolved).toEqual([
          { id: 1, name: 'RAY' },
        ]);
      })
      .schematize();

    const source = `
      query {
        employeeList {
          id
          name
        }
      }
    `;
    const contextValue = { data };

    const result = await graphql({
      schema: schemaWithListeners,
      source,
      contextValue,
    });

    expect(result.errors).toBeUndefined();
    expect(result.data).toEqual({
      employeeList: [
        { id: 1, name: 'RAY' },
      ],
    });
    expect(onSelectSecuence).toBe(1);
    expect(onResolveSecuence).toBe(2);
    expect(onResolveTwoSecuence).toBe(3);
  });
});
