import { employeeMutations } from './employeeMutations';
import { saleMutations } from './saleMutations';

const mutations = [employeeMutations, saleMutations];

export default mutations;
