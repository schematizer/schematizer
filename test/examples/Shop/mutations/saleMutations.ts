import { Arg, Fn, Mutations } from '../../../../src';
import { saleAdd } from '../core';
import { SaleInput } from '../inputs';
import { SaleType } from '../types';

export const saleMutations = new Mutations({
  saleAdd: Fn(SaleType, ({
    input = Arg(SaleInput),
  }) => ({
    context,
  }) => {
    return saleAdd(context.data, input);
  }),
});
