import { Arg, Fn, Mutations } from '../../../../src';
import { employeeAdd } from '../core';
import { EmployeeInput } from '../inputs';
import { EmployeeType } from '../types';

export const employeeMutations = new Mutations({
  employeeAdd: Fn(EmployeeType, ({
    input = Arg(EmployeeInput),
  }) => ({
    context,
  }) => {
    return employeeAdd(context.data, input);
  }),
});
