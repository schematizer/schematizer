import { Fn, Queries } from '../../../../src';
import { salesGet } from '../core/salesGet';
import { SaleType } from '../types/SaleType';

export const saleQueries = new Queries({
  salesList: Fn([SaleType], () => ({ context }) => {
    const result = salesGet(context.data);
    return result;
  }),
});
