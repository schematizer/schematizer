import { employeeQueries } from './employeeQueries';
import { saleQueries } from './saleQueries';

const queries = [employeeQueries, saleQueries];

export default queries;
