import { Fn, Queries } from '../../../../src';
import { Data } from '../data';
import { EmployeeType } from '../types';

export const employeeQueries = new Queries({
  employeeList: Fn([EmployeeType], () => ({
    context,
  }) => {
    const data: Data = context.data;
    return data.employees;
  }),
});
