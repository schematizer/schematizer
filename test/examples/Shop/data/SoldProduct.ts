export interface SoldProduct {
  id: number;
  name: string;
  quantity: number;
  saleId: number;
}
