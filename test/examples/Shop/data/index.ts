import { Employee } from './Employee';
import { Sale } from './Sale';
import { SoldProduct } from './SoldProduct';

export * from './Employee';
export * from './Sale';
export * from './SoldProduct';

export interface DataInfo {
  employees: Employee[];
  sales: Sale[];
  soldProducts: SoldProduct[];
}

export class Data implements DataInfo {
  public employees: Employee[] = [];
  public sales: Sale[] = [];
  public soldProducts: SoldProduct[] = [];

  constructor(initialInfo?: Partial<DataInfo>) {
    if (!initialInfo) {
      return;
    }
    Object.assign(this, initialInfo);
  }
}
