import { Type } from '../../../../src';
import { Data } from '../data';
import { EmployeeType } from './EmployeeType';
import { SoldProductType } from './SoldProductType';

export class SaleType {
  public id: number;
  public by: EmployeeType;
  public products: SoldProductType[];
}

export const saleType = new Type(SaleType).fields(() => ({
  id: {
    type: 'int',
  },
  by: {
    type: EmployeeType,
    resolve: () => ({ context, root }) => {
      const data: Data = context.data;
      const saleInfo = data.sales.find(sale => sale.id === root.id);
      if (!saleInfo) {
        return null;
      }
      const employee = data.employees.find(employeeInfo => employeeInfo.id === saleInfo.employeeId);

      return employee;
    },
  },
  products: {
    type: [SoldProductType],
    resolve: () => ({ context, root }) => {
      const data: Data = context.data;

      const products: SoldProductType[] = data.soldProducts
        .filter(product => product.saleId === root.id);

      return products;
    },
  },
  }));
