import { employeeType } from './EmployeeType';
import { saleType } from './SaleType';
import { soldProductType } from './SoldProductType';

export * from './EmployeeType';
export * from './SaleType';
export * from './SoldProductType';

const types = [employeeType, saleType, soldProductType];

export default types;
