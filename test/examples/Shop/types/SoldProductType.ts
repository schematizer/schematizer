import { Type } from '../../../../src';

export class SoldProductType {
  public id: number;
  public name: string;
  public quantity: number;
}

export const soldProductType = new Type(SoldProductType).fields(() => ({
  id: 'int',
  name: 'string',
  quantity: 'float',
}));
