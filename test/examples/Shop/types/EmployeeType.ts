import { Type } from '../../../../src';

export class EmployeeType {
  public id: number;
  public name: string;
}

export const employeeType = new Type(EmployeeType).fields(() => ({
  id: 'int',
  name: 'string',
}));
