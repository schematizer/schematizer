import { Input } from '../../../../src';
import { SoldProductInput } from './SoldProductInput';

export class SaleInput {
  public by: number;
  public products: SoldProductInput[];
}

export const saleInput = new Input(SaleInput).fields(() => ({
  by: 'int',
  products: [SoldProductInput],
}));
