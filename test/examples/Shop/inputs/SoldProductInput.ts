import { Input } from '../../../../src';

export class SoldProductInput {
  public name: string;
  public quantity: number;
}

export const soldProductInput = new Input(SoldProductInput).fields(() => ({
  name: 'string',
  quantity: 'float',
}));
