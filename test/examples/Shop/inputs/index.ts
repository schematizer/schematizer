import { employeeInput } from './EmployeeInput';
import { saleInput } from './SaleInput';
import { soldProductInput } from './SoldProductInput';

export * from './EmployeeInput';
export * from './SaleInput';
export * from './SoldProductInput';

const inputs = [employeeInput, saleInput, soldProductInput];

export default inputs;
