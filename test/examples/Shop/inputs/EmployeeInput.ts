import { Input } from '../../../../src';

export class EmployeeInput {
  public name: string;
}

export const employeeInput = new Input(EmployeeInput).fields(() => ({
  name: 'string',
}));
