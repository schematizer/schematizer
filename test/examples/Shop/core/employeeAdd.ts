import { Data, Employee } from '../data';
import { EmployeeInput } from '../inputs';

export function employeeAdd(data: Data, input: EmployeeInput) {
  const employee: Employee = {
    id: data.employees.length + 1,
    name: input.name,
  };

  data.employees.push(employee);

  return employee;
}
