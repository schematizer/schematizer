import { Data, Sale, SoldProduct } from '../data';
import { SoldProductInput } from '../inputs';

export function soldProductAdd(data: Data, sale: Sale, input: SoldProductInput) {
  const soldProduct: SoldProduct = {
    id: data.soldProducts.length + 1,
    name: input.name,
    quantity: input.quantity,
    saleId: sale.id,
  };

  data.soldProducts.push(soldProduct);

  return soldProduct;
}
