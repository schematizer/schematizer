import { Data, Sale } from '../data';
import { SaleInput } from '../inputs';
import { soldProductAdd } from './soldProductAdd';

export function saleAdd(data: Data, input: SaleInput) {
  const sale: Sale = {
    employeeId: input.by,
    id: data.sales.length + 1,
  };

  data.sales.push(sale);

  input.products.forEach(product => soldProductAdd(data, sale, product));

  return sale;
}
