import { Arg, Fn, Mutations } from '../../src';

it('simple declaration', () => {
  class ProductInput {}
  class EmployeeInput {}
  class Product {}
  class Employee {}

  const mutations = new Mutations({
    products: Fn(Product, ({
      input = Arg(ProductInput),
    }) => () => {
      return {};
    }),
    employees: Fn(Employee, ({
      input = Arg(EmployeeInput),
    }) => () => {
      return {};
    }),
  });

  expect(mutations).toBeInstanceOf(Mutations);
});
