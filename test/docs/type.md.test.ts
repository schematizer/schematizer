import { Type } from '../../src';

it ('simple type', () => {
  class Recipe {
    public id: string;
    public title: string;
    public views: number;
    public visible: boolean;
  }

  const recipeType = new Type(Recipe)
    .as('RecipeType')
    .fields(() => ({
      id: 'id',
      title: 'string',
      views: 'int',
      visible: 'boolean',
    }));

  expect(recipeType).toBeInstanceOf(Type);
});

it('field types', () => {
  class Ingredient {}
  class User {}

  class Recipe {
    public title: string;
    public owner: User;
    public tags: string[];
    public ingredients: Ingredient[];
  }

  const recipeType = new Type(Recipe).fields(() => ({
    title: 'string', // scalar
    owner: User, // nested type
    tags: ['string'], // array of scalars
    ingredients: [Ingredient], // array of typed
  }));

  expect(recipeType).toBeInstanceOf(Type);
});

it('field options', () => {
  class Charger {}
  class Phone {
    public name: string;
    public charger: Charger;
  }

  const phoneType = new Type(Phone).fields(() => ({
    name: {
      type: 'string',
    },
    charger: {
      type: Charger,
      nullable: true,
    },
  }));

  expect(phoneType).toBeInstanceOf(Type);
});
