import { Input } from '../../src';

it('simple input', () => {
  class ToDo {
    public name: string;
    public description: string;
  }

  const toDoInput = new Input(ToDo)
    .as('ToDoInput')
    .fields(() => ({
      name: 'string',
      description: 'string',
    }));
  expect(toDoInput).toBeInstanceOf(Input);
});

it('field types', () => {
  class ToDoInput {}

  class ToDoListInput {
    public name: string;
    public assignedTo: number[];
    public items: ToDoInput[];
  }

  const toDoListInput = new Input(ToDoListInput).fields(() => ({
    name: 'string',
    assignedTo: ['int'],
    items: [ToDoInput],
  }));
  expect(toDoListInput).toBeInstanceOf(Input);
});

it('field options', () => {
  class VariantInput {}

  class ProductInput {
    public name: string;
    public variants: VariantInput[];
  }

  const productInput = new Input(ProductInput).fields(() => ({
    name: {
      type: 'string',
    },
    variants: {
      type: [VariantInput],
      nullable: true,
    },
  }));
  expect(productInput).toBeInstanceOf(Input);
});
