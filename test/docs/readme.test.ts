import { graphql } from 'graphql';
import { Arg, Fn, Input, Mutations, Queries, schematize, Type } from '../../src';

it('how this library works', async () => {
  class Product {
    public id: number;
    public name: string;
    public price: number;
    public salable: boolean;
  }

  const productType = new Type(Product).fields(() => ({
    id: 'int',
    name: 'string',
    price: 'float',
    salable: 'boolean',
  }));

  const products: Product[] = [
    { id: 1, name: 'foo', price: 5, salable: true },
    { id: 2, name: 'bar', price: 2, salable: true },
  ];

  // make queries

  const queries = new Queries({
    products: Fn([Product], () => () => products),
  });

  // schematize

  const schema = schematize(productType, queries);
  const source = `
    query {
      products {
        id
        name
        price
        salable
      }
    }
  `;

  // you can use your favorite server like "graphql yoga" or "apollo server"
  const { data, errors } = await graphql({ schema, source });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    products: [
      { id: 1, name: 'foo', price: 5, salable: true },
      { id: 2, name: 'bar', price: 2, salable: true },
    ],
  });
});

it('type', () => {
  class Person {
    public id: number;
    public name: string;
  }

  const personType = new Type(Person)
    .as('userType') // optional
    .fields(() => ({
      id: 'int',
      name: 'string',
    }));

  expect(personType).toBeInstanceOf(Type);
});

it('input', () => {

  class MovementInput {
    public from: number;
    public to: number;
    public amount: number;
    public description: string;
  }

  const movementInput = new Input(MovementInput).fields(() => ({
    from: 'int',
    to: 'int',
    amount: 'float',
    description: 'string',
  }));
  expect(movementInput).toBeInstanceOf(Input);
});

it('queries', () => {
  class Product {}
  const getProductsMagically = (sql: string) => [new Product()];
  const getProductMagically = (id: number) => new Product();

  const queries = new Queries({
    getProducts: Fn([Product], ({
      sqlInject = Arg('string'), // do not do this
    }) => () => {
      return getProductsMagically(sqlInject);
    }),

    getProduct: Fn(Product, ({
      id = Arg('int'),
    }) => () => {
      return getProductMagically(id);
    }),
  });

  expect(queries).toBeInstanceOf(Queries);
});

it('mutations', () => {
  class Product {}
  class ProductInput {}

  const addProductMagically = (input: ProductInput) => new Product();
  const removeProductMagically = (id: number) => new Product();

  const mutations = new Mutations({
    addProduct: Fn(Product, ({
      input = Arg(ProductInput),
    }) => () => {
      return addProductMagically(input);
    }),

    removeProduct: Fn(Product, ({
      id = Arg('int'),
    }) => () => {
      return removeProductMagically(id);
    }),
  });

  expect(mutations).toBeInstanceOf(Mutations);
});
