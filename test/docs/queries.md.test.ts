import { Fn, Queries } from '../../src';

it('simple declaration', () => {
  class Product {}
  class Employee {}

  const queries = new Queries({
    products: Fn([Product], () => () => [{}, {}, {}, {}]),
    employees: Fn([Employee], () => () => [{}, {}, {}, {}]),
  });

  expect(queries).toBeInstanceOf(Queries);
});
