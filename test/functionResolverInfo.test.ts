import { graphql } from 'graphql';
import { Fn, FunctionSelection, Queries, schematize } from '../src';
import { TypeSchematizer } from '../src/schematizers';
import { TodoListType, todoListType } from './types/TodoListType';
import { todoType, TodoType } from './types/TodoType';
import { userType, UserType } from './types/UserType';

it('selection info', async () => {
  let selection: FunctionSelection = null as any;
  const queries = new Queries({
    getTodoList: Fn(TodoListType , () => info => {
      selection = info.selection;
      return {
        title: 'Foo',
        todos: [
          {
            title: 'One',
            done: false,
            assigned: {
              name: 'Ray',
            },
          },
          {
            title: 'Two',
            done: false,
          },
        ],
      };
    }),
  });

  const schema = schematize(userType, todoType, todoListType, queries);
  const source = `
    query {
      getTodoList {
        title
        todos {
          title
          done
          assigned {
            name
          }
        }
      }
    }
  `;

  const { data, errors } = await graphql({ schema, source });

  expect(data).toEqual({
    getTodoList: {
      title: 'Foo',
      todos: [
        {
          title: 'One',
          done: false,
          assigned: {
            name: 'Ray',
          },
        },
        {
          title: 'Two',
          done: false,
          assigned: null,
        },
      ],
    },
  });
  expect(errors).toBeUndefined();

  expect(selection).toEqual(expect.objectContaining({
    fn: queries.functions.getTodoList,
    selection: {
      type: new TypeSchematizer(todoListType),
      info: {
        nullable: false,
        onSelect: [],
        resolve: undefined,
        type: TodoListType,
        raw: undefined,
      },
      fields: {
        title: {
          type: 'string',
          info: {
            nullable: false,
            onSelect: [],
            resolve: undefined,
            type: 'string',
          },
          raw: undefined,
        },
        todos: {
          type: new TypeSchematizer(todoType),
          info: expect.objectContaining({
            nullable: false,
            onSelect: [],
            resolve: undefined,
            type: [TodoType],
          }),
          fields: {
            title: {
              type: 'string',
              info: {
                nullable: false,
                onSelect: [],
                resolve: undefined,
                type: 'string',
              },
              raw: undefined,
            },
            done: {
              type: 'boolean',
              info: {
                nullable: false,
                onSelect: [],
                resolve: undefined,
                type: 'boolean',
              },
              raw: undefined,
            },
            assigned: {
              type: new TypeSchematizer(userType),
              info: {
                nullable: true,
                onSelect: [],
                resolve: undefined,
                type: UserType,
              },
              fields: {
                name: {
                  type: 'string',
                  info: {
                    nullable: false,
                    onSelect: [],
                    resolve: undefined,
                    type: 'string',
                    raw: undefined,
                  },
                },
              },
              raw: expect.any(Object),
            },
          },
          raw: expect.any(Object),
        },
      },
      raw: expect.any(Object),
    },
    raw: expect.any(Object),
  }));
});
