import { Type } from '../../src';
import { TodoType } from './TodoType';

export class UserType {
  public name: string;
  public assignments: TodoType[];
}

export const userType = new Type(UserType).fields(() => ({
  name: 'string',
  assignments: [TodoType],
}));
