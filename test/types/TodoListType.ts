import { Type } from '../../src';
import { TodoType } from './TodoType';

export class TodoListType {
  public title: string;
  public todos: TodoType[];
}

export const todoListType = new Type(TodoListType).fields(() => ({
  title: 'string',
  todos: [TodoType],
}));
