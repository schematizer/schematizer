import { Type } from '../../src';
import { TodoListType } from './TodoListType';
import { UserType } from './UserType';

export class TodoType {
  public done: boolean;
  public title: string;
  public list: TodoListType;
  public assigned: UserType;
}

export const todoType = new Type(TodoType).fields(() => ({
  done: 'boolean',
  title: 'string',
  list: TodoListType,
  assigned: { type: UserType, nullable: true },
}));
