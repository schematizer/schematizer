import { graphql } from 'graphql';
import { Fn, Mutations, Queries, schematize, Type } from '../../src';

it('query listeners', async () => {
  const fooHandler = jest.fn(() => { /* do nothing*/ });
  const barHandler = jest.fn(() => { /* do nothing*/ });

  const queries = new Queries({
    foo: Fn('string', () => () => 'hello foo world').onSelect(fooHandler),
    bar: Fn('string', () => () => 'hello bar world').onSelect(barHandler),
  });

  const schema = schematize(queries);
  const source = `
    query {
      foo
      bar
      FOO: foo
      BAR: bar
    }
  `;

  const { data, errors } = await graphql({
    schema,
    source,
  });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    foo: 'hello foo world',
    bar: 'hello bar world',
    FOO: 'hello foo world',
    BAR: 'hello bar world',
  });

  expect(fooHandler.mock.calls.length).toBe(2);
  expect(barHandler.mock.calls.length).toBe(2);
});

it('query listeners', async () => {
  const fooHandler = jest.fn(() => { /* do nothing*/ });
  const barHandler = jest.fn(() => { /* do nothing*/ });

  const queries = new Queries({
    test: Fn('string', () => () => 'test'),
  });

  const mutations = new Mutations({
    foo: Fn('string', () => () => 'hello foo world').onSelect(fooHandler),
    bar: Fn('string', () => () => 'hello bar world').onSelect(barHandler),
  });

  const schema = schematize(queries, mutations);
  const source = `
    mutation {
      foo
      bar
      FOO: foo
      BAR: bar
    }
  `;

  const { data, errors } = await graphql({
    schema,
    source,
  });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    foo: 'hello foo world',
    bar: 'hello bar world',
    FOO: 'hello foo world',
    BAR: 'hello bar world',
  });

  expect(fooHandler.mock.calls.length).toBe(2);
  expect(barHandler.mock.calls.length).toBe(2);
});

it('type and fields listeners', async () => {

  class Ingredient {
    public name: string = '';
  }

  class Recipe {
    public title: string = '';
    public ingredients: Ingredient[] = [];
  }

  const recipeTypeListener = jest.fn(() => { /* do nothing */ });
  const titleFieldListener = jest.fn(() => { /* do nothing*/ });
  const ingredientsFieldListener = jest.fn(() => { /* do nothing */ });

  const ingredientTypeListener = jest.fn(() => { /* do nothing */ });
  const nameFieldListener = jest.fn(() => { /* do nothing */ });

  const recipeType = new Type(Recipe)
    .onSelect(recipeTypeListener)
    .fields(() => ({
      title: {
        type: 'string',
        onSelect: titleFieldListener,
      },
      ingredients: {
        type: [Ingredient],
        onSelect: ingredientsFieldListener,
      },
    }));

  const ingredientType = new Type(Ingredient)
    .onSelect(ingredientTypeListener)
    .fields(() => ({
      name: {
        type: 'string',
        onSelect: nameFieldListener,
      },
    }));

  const queries = new Queries({
    getRecipe: Fn(Recipe, () => () => {
      return new Recipe();
    }),
    getIngredient: Fn(Ingredient, () => () => {
      return new Ingredient();
    }),
  });

  const schema = schematize(ingredientType, recipeType, queries);
  const source = `
    query {
      getRecipe {
        title
        ingredients {
          name
        }
      }
      getIngredient {
        name
      }
    }
  `;

  const { data, errors } = await graphql({ schema, source });
  expect(errors).toBeUndefined();
  expect(data).toEqual({
    getRecipe: {
      title: '',
      ingredients: [],
    },
    getIngredient: {
      name: '',
    },
  });

  expect(recipeTypeListener.mock.calls.length).toBe(1);
  expect(titleFieldListener.mock.calls.length).toBe(1);
  expect(ingredientsFieldListener.mock.calls.length).toBe(1);

  // TODO: Optimize calls
  expect(ingredientTypeListener.mock.calls.length).toBe(2);
  expect(nameFieldListener.mock.calls.length).toBe(2);
});
