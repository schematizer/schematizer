import { graphql } from 'graphql';
import { Fn, GraphqlContext, Queries, schematize } from '../src';

declare module '../src/types' {
  interface GraphqlContext {
    flag: boolean;
  }
}

it ('check context', async () => {
  const queries = new Queries({
    foo: Fn('string', () => ({
      context,
    }) => {
      expect(context.flag).toBeTruthy();
      return 'bar';
    }),
  });

  const schema = schematize(queries);
  const contextValue: GraphqlContext = {
    flag: true,
  };
  const source = `
    query {
      foo
    }
  `;

  const { data, errors } = await graphql({ source, schema, contextValue });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    foo: 'bar',
  });
});
